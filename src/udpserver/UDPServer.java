/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package udpserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jideesh
 */
public class UDPServer 
{
    public static InputStream is; 
    public static OutputStream os = null;
    public static void main(String[] args) throws Exception 
    {
        final int LOCAL_PORT = 1235;
        String SERVER_NAME = "192.168.100.107";
        
        
        String systemipaddress = ""; 
        
        
        try
        { 
            String dir = System.getProperty("user.dir");
            dir = "/home/jideesh/web/recordings_steth";
            
            File directory = new File(dir);
            if(!directory.exists())
            {
                directory.mkdir();
                System.out.println("Directory created");
            }
            System.out.println(dir);
            URL url_name = new URL("https://wtfismyip.com/text"); 
  
            BufferedReader sc = new BufferedReader(new InputStreamReader(url_name.openStream())); 
  
            // reads system IPAddress 
            systemipaddress = sc.readLine().trim(); 
            SERVER_NAME = SERVER_NAME;
        } 
        catch (Exception e) 
        { 
            systemipaddress = "Cannot Execute Properly"; 
        } 
        
        System.out.println("Public IP Address: " + systemipaddress +"\n"); 
         
        DatagramSocket udpSocket = new DatagramSocket(LOCAL_PORT);
        ServerSocket soc = new ServerSocket(LOCAL_PORT);
        
        Thread t = new Thread()
        {
            public void run()
            {
                byte[] b = new byte[1000];
                try {
                    Socket socket = soc.accept();
                    is = socket.getInputStream();
                    os = socket.getOutputStream();
                    while(socket.getInputStream().read(b)>0)
                    {
                        
                    }
                } catch (IOException ex) {
                    Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        };
        t.start();
        
//        Socket soc = new Socket(1235);
        System.out.println("Created UDP  server socket at "+ udpSocket.getLocalSocketAddress() + "...");
        while (true) 
        {
            System.out.println("Waiting for a  UDP  packet...");
            DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);
            udpSocket.receive(packet);
            displayPacketDetails(packet);
            DatagramPacket sendPacket = new DatagramPacket(packet.getData(), packet.getLength(),packet.getAddress(),packet.getPort());
            udpSocket.send(sendPacket);
        }
    }
    
    public static void displayPacketDetails(DatagramPacket packet) 
    {
        byte[] msgBuffer = packet.getData();
        int length = packet.getLength();
        int offset = packet.getOffset();

        int remotePort = packet.getPort();
        InetAddress remoteAddr = packet.getAddress();
        String msg = new String(msgBuffer, offset, length);
        System.out.println("Received a  packet:[IP Address=" + remoteAddr+ ", port=" + remotePort + ", message=" + msg + "]");
    }
}
